#!/bin/bash
# We got match! This is zip file in fact.
hexdump -C "$(dirname $0)/foo.ico" | grep "50 4b 03 04"

# Unzip it!
unzip "$(dirname $0)/foo.ico"
